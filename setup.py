from setuptools import setup
from Cython.Build import cythonize

setup(
    # name='EVTC Parser',
    ext_modules=cythonize("evtc.pyx", annotate=True, compiler_directives={"profile": True}),
    zip_safe=False,
)
