cdef class EVTCAgent:
    cdef readonly unsigned long long addr
    cdef readonly unsigned int prof
    cdef readonly unsigned int is_elite
    cdef readonly unsigned short toughness
    cdef readonly unsigned short concentration
    cdef readonly unsigned short healing
    cdef readonly unsigned short hitbox_width
    cdef readonly unsigned short condition
    cdef readonly unsigned short hitbox_height
    cdef readonly char[68] _name
    cdef readonly unsigned short instid
    cdef readonly unsigned long long first_aware
    cdef readonly unsigned long long last_aware
    cdef readonly unsigned long long master_addr

    @staticmethod
    cdef from_evtc(char * data)

cdef class EVTCSkill:
    cdef readonly unsigned int id
    cdef readonly char[64] _name

    @staticmethod
    cdef from_evtc(char * data)

cpdef enum CombatResult:
    CBTR_NORMAL
    CBTR_CRIT
    CBTR_GLANCE
    CBTR_BLOCK
    CBTR_EVADE
    CBTR_INTERRUPT
    CBTR_ABSORB
    CBTR_BLIND
    CBTR_KILLING_BLOW
    CBTR_DOWNED
    CBTR_BREAKBAR
    CBTR_ACTIVATION
    CBTR_UNKNOWN


cpdef enum CombatActivation:
    ACTV_NONE
    ACTV_START
    ACTV_QUICKNESS_UNUSED
    ACTV_CANCEL_FIRE
    ACTV_CANCEL_CANCEL
    ACTV_RESET
    ACTV_UNKNOWN


cpdef enum CombatStateChange:
    CBTS_NONE
    CBTS_ENTER_COMBAT
    CBTS_EXIT_COMBAT
    CBTS_CHANGE_UP
    CBTS_CHANGE_DEAD
    CBTS_CHANGE_DOWN
    CBTS_SPAWN
    CBTS_DESPAWN
    CBTS_HEALTH_UPDATE
    CBTS_LOG_START
    CBTS_LOG_END
    CBTS_WEAPSWAP
    CBTS_MAX_HEALTH_UPDATE
    CBTS_POINT_OF_VIEW
    CBTS_LANGUAGE
    CBTS_GW_BUILD
    CBTS_SHARD_ID
    CBTS_REWARD
    CBTS_BUFF_INITIAL
    CBTS_POSITION
    CBTS_VELOCITY
    CBTS_FACING
    CBTS_TEAM_CHANGE
    CBTS_ATTACK_TARGET
    CBTS_TARGETABLE
    CBTS_MAP_ID
    CBTS_REPL_INFO
    CBTS_STACK_ACTIVE
    CBTS_STACK_RESET
    CBTS_GUILD
    CBTS_BUFF_INFO
    CBTS_BUFF_FORMULA
    CBTS_SKILL_INFO
    CBTS_SKILL_TIMING
    CBTS_BREAKBAR_STATE
    CBTS_BREAKBAR_PERCENT
    CBTS_ERROR
    CBTS_TAG
    CBTS_BARRIER_UPDATE
    CBTS_STAT_RESET
    CBTS_EXTENSION
    CBTS_API_DELAYED
    CBTS_UNKNOWN


cdef class EVTCEventRv1:
    cdef readonly unsigned long long time
    cdef readonly unsigned long long src_agent
    cdef readonly unsigned long long dst_agent
    cdef readonly signed int value
    cdef readonly signed int buff_dmg
    cdef readonly unsigned int overstack_value
    cdef readonly unsigned int skillid
    cdef readonly unsigned short src_instid
    cdef readonly unsigned short dst_instid
    cdef readonly unsigned short src_master_instid
    cdef readonly unsigned short dst_master_instid
    cdef readonly unsigned char iff
    cdef readonly unsigned char buff
    cdef readonly unsigned char result
    cdef readonly unsigned char is_activation
    cdef readonly unsigned char is_buffremove
    cdef readonly unsigned char is_ninety
    cdef readonly unsigned char is_fifty
    cdef readonly unsigned char is_moving
    cdef readonly unsigned char is_statechange
    cdef readonly unsigned char is_flanking
    cdef readonly unsigned char is_shields
    cdef readonly unsigned char is_offcycle
    cdef readonly unsigned char pad61
    cdef readonly unsigned char pad62
    cdef readonly unsigned char pad63
    cdef readonly unsigned char pad64

    @staticmethod
    cdef from_evtc(char * data)
