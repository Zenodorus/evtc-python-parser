import zipfile
from typing import Optional, List, Type, Dict
from libc.string cimport memcpy


cdef class EVTCAgent:

    @staticmethod
    cdef from_evtc(char * data):
        cdef EVTCAgent self = EVTCAgent.__new__(EVTCAgent)
        self.addr =          (<unsigned long long*>data)[0]
        self.prof =          (<unsigned int*>      (data + 8))[0]
        self.is_elite =      (<unsigned int*>      (data + 12))[0]
        self.toughness =     (<unsigned short*>    (data + 16))[0]
        self.concentration = (<unsigned short*>    (data + 18))[0]
        self.healing =       (<unsigned short*>    (data + 20))[0]
        self.hitbox_width =  (<unsigned short*>    (data + 22))[0]
        self.condition =     (<unsigned short*>    (data + 24))[0]
        self.hitbox_height = (<unsigned short*>    (data + 26))[0]
        memcpy(self._name, data + 28, 68)
        self.instid = self.first_aware = self.last_aware = self.master_addr = 0
        return self

    @property
    def name(self):
        return self._name[:68]

    @property
    def name_decoded(self):
        return self._name[:68].decode("utf-8")

    @property
    def player_name(self) -> Optional[str]:
        if self.is_player:
            return self.name_decoded.split('\0')[0]
        return None

    @property
    def account_name(self) -> Optional[str]:
        if self.is_player:
            return self.name_decoded.split('\0')[1]
        return None

    @property
    def subgroup(self) -> Optional[str]:
        if self.is_player:
            return self.name_decoded.split('\0')[2]
        return None

    @property
    def is_player(self) -> bool:
        return self.is_elite != 0xFFFFFFFF

    @property
    def is_gadget(self) -> bool:
        return not self.is_player and self.prof & 0xFFFF0000 == 0xFFFF0000

    @property
    def is_npc(self) -> bool:
        return not self.is_player and self.prof & 0xFFFF0000 != 0xFFFF0000

    @property
    def npc_id(self) -> int:
        return self.prof & 0xFFFF

    def __repr__(self):
        return f"<EVTCAgent {self.name_decoded} at {self.addr:#010x}>"


cdef class EVTCSkill:

    @staticmethod
    cdef from_evtc(char * data):
        cdef EVTCSkill self = EVTCSkill.__new__(EVTCSkill)
        self.id = (<unsigned int*>data)[0]
        memcpy(self._name, data + 4, 64)
        return self


EVTCEventRv1_fields = ("time", "src_agent", "dst_agent", "value", "buff_dmg", "overstack_value",
                                           "skillid", "src_instid", "dst_instid", "src_master_instid",
                                           "dst_master_instid", "iff", "buff", "result", "is_activation",
                                           "is_buffremove", "is_ninety", "is_fifty", "is_moving", "is_statechange",
                                           "is_flanking", "is_shields", "is_offcycle")


cdef class EVTCEventRv1:

    @staticmethod
    cdef from_evtc(char * data):
        cdef EVTCEventRv1 self = EVTCEventRv1.__new__(EVTCEventRv1)
        self.time                = (<unsigned long long*>data)[0]
        self.src_agent           = (<unsigned long long*>(data + 8))[0]
        self.dst_agent           = (<unsigned long long*>(data + 16))[0]
        self.value               = (<signed int*>        (data + 24))[0]
        self.buff_dmg            = (<signed int*>        (data + 28))[0]
        self.overstack_value     = (<unsigned int*>      (data + 32))[0]
        self.skillid             = (<unsigned int*>      (data + 36))[0]
        self.src_instid          = (<unsigned short*>    (data + 40))[0]
        self.dst_instid          = (<unsigned short*>    (data + 42))[0]
        self.src_master_instid   = (<unsigned short*>    (data + 44))[0]
        self.dst_master_instid   = (<unsigned short*>    (data + 46))[0]
        self.iff                 = (<unsigned char*>     (data + 48))[0]
        self.buff                = (<unsigned char*>     (data + 49))[0]
        self.result              = (<unsigned char*>     (data + 50))[0]
        self.is_activation       = (<unsigned char*>     (data + 51))[0]
        self.is_buffremove       = (<unsigned char*>     (data + 52))[0]
        self.is_ninety           = (<unsigned char*>     (data + 53))[0]
        self.is_fifty            = (<unsigned char*>     (data + 54))[0]
        self.is_moving           = (<unsigned char*>     (data + 55))[0]
        self.is_statechange      = (<unsigned char*>     (data + 56))[0]
        self.is_flanking         = (<unsigned char*>     (data + 57))[0]
        self.is_shields          = (<unsigned char*>     (data + 58))[0]
        self.is_offcycle         = (<unsigned char*>     (data + 59))[0]
        self.pad61               = (<unsigned char*>     (data + 60))[0]
        self.pad62               = (<unsigned char*>     (data + 61))[0]
        self.pad63               = (<unsigned char*>     (data + 62))[0]
        self.pad64               = (<unsigned char*>     (data + 63))[0]
        return self

    @property
    def event_type(self) -> str:
        if self.is_statechange == 6:
            return "AgentSpawnEvent"
        return ""

    def __repr__(self):
        return f"<EVTCEventRv1 src_agent={self.src_agent} at t={self.time}>"


class EVTCError(Exception):
    pass


class EVTCFile(object):

    def __init__(self, filename):
        self._filename: str = filename
        self._data: bytes

        if self._filename.endswith(".evtc"):
            with open(self._filename, 'rb') as f:
                self._data = f.read()
        elif self._filename.endswith(".zevtc") or self._filename.endswith(".evtc.zip"):
            with zipfile.ZipFile(self._filename, mode='r', compression=zipfile.ZIP_DEFLATED) as zevtc:
                if len(zevtc.infolist()) != 1:
                    raise EVTCError("zevtc archive does not have only 1 file in it")
                with zevtc.open(zevtc.infolist()[0].filename, mode='r') as evtc:
                    self._data = evtc.read()
        else:
            raise EVTCError("unsupported file type")

        if self.revision != 1:
            raise EVTCError(f"revision {self.revision} not supported")

        cdef unsigned int num_skills, idx_skills, num_remaining_data, num_events, idx_events, i
        cdef EVTCAgent a

        num_agents = (<unsigned int *> (<char *> self._data + 16))[0]
        self.agents: Dict[int, Type[EVTCAgent]] = {}
        for i in range(num_agents):
            a = EVTCAgent.from_evtc(<char *> self._data + 20 + 96 * i)
            self.agents[a.addr] = a

        idx_skills = 24 + 96 * num_agents
        num_skills = (<unsigned int *> (<char *> self._data + idx_skills - 4))[0]
        self.skills: List[Type[EVTCSkill]] = [EVTCSkill.from_evtc(<char *> self._data + idx_skills + 68 * i)
                                              for i in range(num_skills)]

        num_remaining_data = len(self._data) - (24 + 96 * num_agents + 68 * num_skills)
        if num_remaining_data % 64 != 0:
            raise EVTCError("evtc file has extra data")
        num_events = num_remaining_data // 64
        idx_events = 24 + 96 * num_agents + 68 * num_skills
        self.events: List[Type[EVTCEventRv1]] = [EVTCEventRv1.from_evtc(<char *> self._data + idx_events + 64 * i)
                                                 for i in range(num_events)]

        self._log_start_tgt = self._log_end_tgt = 0
        self.process()

    @property
    def filename(self) -> str:
        return self._filename

    @property
    def data(self) -> bytes:
        return self._data

    @property
    def num_agents(self) -> int:
        return len(self.agents)

    @property
    def num_skills(self) -> int:
        return len(self.skills)

    @property
    def num_events(self) -> int:
        return len(self.events)

    @property
    def version(self) -> bytes:
        return self.data[:12]

    @property
    def revision(self) -> int:
        return self.data[12]

    @property
    def boss_id(self) -> int:
        return int.from_bytes(self.data[13:15], byteorder="little")

    @property
    def log_start_tgt(self) -> int:
        return self._log_start_tgt

    @property
    def log_end_tgt(self) -> int:
        return self._log_end_tgt

    def process(self):
        cdef EVTCAgent a, minion
        cdef EVTCEventRv1 e

        for e in self.events:
            if e.is_statechange == CBTS_NONE and e.src_agent in self.agents:
                a = self.agents[e.src_agent]
                a.instid = e.src_instid
                if not a.first_aware:
                    a.first_aware = e.time
                a.last_aware = e.time
            elif e.is_statechange == CBTS_LOG_START:
                if e.src_agent != 0x637261:
                    raise EVTCError("log start event has incorrect src_agent")
                self._log_start_tgt = e.time
            elif e.is_statechange == CBTS_LOG_END:
                if e.src_agent != 0x637261:
                    raise EVTCError("log end event has incorrect src_agent")
                self._log_end_tgt = e.time


        agents_id: Dict[int, List[Type[EVTCAgent]]] = {}
        for a in self.agents.values():
            if a.instid not in agents_id:
                agents_id[a.instid] = []
            agents_id[a.instid].append(a)

        count = 0
        for e in self.events:
            if e.src_master_instid != 0 and e.src_master_instid in agents_id:
                for a in agents_id[e.src_master_instid]:
                    if a.first_aware <= e.time <= a.last_aware:
                        minion = self.agents[e.src_agent]
                        minion.master_addr = a.addr
